# Sujet 2 : Le Web est-il devenu trop compliqué ?
Mathis LEFEBVRE
## Qui comprend internet et le web aujourd'hui ?

Personne. Il ne semble pas raisonnable aujourd'hui, pour quiconque de prétendre comprendre le fonctionnement d'internet et du web **dans sa globalité**. Bien sur, des personnes très compétentes, expertes de leur domaine existent. Ces experts ont même sans doute une assez bonne idée du fonctionnement de la plupart des technologies liées à internet et au web. Mais il serait déraisonnable de prétendre **tout** savoir sur le web.
En restant très général, l'architecture d'internet pourrait être découpée en couches. On aurait alors l'idée suivante : 
* Une couche physique : Les câbles et matériels qui permettent de faire transiter les bits de données
* Une couche protocolaire : Des normes, logiciels et équipement qui traitent les bits de données pour rendre des services dans les communication, comme assurer l'intégrité, ou le *routage* (Ces protocoles s'organisent eux aussi en plusieurs couches...)
* Une couche applicative : Des programmes et logiciels qui utilisent l'information finale pour réaliser des services utiles aux utilisateurs (un navigateur web par exemple)

Chacune de ces parties utilisent des technologies complexes et variées.
Le problème d'internet à la base, c'est que c'est décentralisé. Bien sur, la chose qui rend intéressant internet à la base, c'est que c'est décentralisé. Ce *problème* évoqué n'est pas d'ordre moral ou éthique, c'est très bien de ne pas centraliser un service, mais c'est compliqué.
C'est compliqué car chacun peut alors faire comme il veut, et malheureusement, dans ces situations.. personne ne fait jamais pareil. Il faut alors normer, parce que le web et internet c'est avant tout une affaire de communication : Si on ne sait pas comment l'autre parle, on ne le comprend pas.
Alors des **grandes organisations** publient des normes, puis d'autres organisation en publient des concurrentes, puis les normes évoluent rapidement parfois, donc les anciennes norment ne sont plus à jour...
Effectivement, ca fonctionne, mais à quel prix ? Avec tout ce processus on semble avoir perdu une partie de la décentralisation, qui justifiait tout cela à la base (Le mot "grandes organisations" n'est pas innocent...).

En essayant de simplifier et de comprendre le fonctionnement, non seulement peu de progrès semble avoir été fait (c'est toujours compliqué), mais en plus, des situations de quasi monopole ont été offert à de grands groupes. les fournisseurs d'accès contrôlent le réseau physique, de grandes associations contrôlent les protocoles, et des géants du web contrôlent les applications. Et l'utilisateur est toujours aussi perdu dans le fonctionnement derrière tout ça. On semble être actuellement dans le pire des 2 mondes.

On retrouve derrière cette analyse un peu pessimiste le commentaire de Stéphane Bortzmeyer et même la réflexion de Tom. Internet est si compliqué qu'on en a perdu la variété, qui est à l'origine même de cette complexité. Et les acteurs qui restent ont profité de ce monopole pour transformer les services selon leurs besoins, d'où la phrase : 
> votre navigateur Web, qui travaille au moins autant pour l'industrie publicitaire que pour vous.

## Que faire maintenant ?

Deux choix : Assumer pleinement le monopole des géants, leur donner plein pouvoir et espérer qu'ils fourniront un service d'une qualité exceptionnelle, au détriment de notre vie privée et de nos libertés, ou bien revenir un peu en arrière et refaire les choses un peu mieux.

C'est plutôt la deuxième idée que défend Stéphane Bortzmeyer en parlant du projet Gemini. Finalement, dans la plupart des cas, nous n'avons pas besoin d'un service aussi complexe, alors autant utiliser un service simple qui ne nous cache rien. Bien sur tout ne faire pas rêver, moins d'esthétique, moins de fonctionnalités... Mais ce n'est pas une fin en soit : l'esthétique et le fonctionnel ne sont pas à bannir, seulement à rajouter lorsque nécessaire. 
Le principe de chaîne éditoriale  devient alors intéressant. Ce qui compte à la base, c'est l'information. Alors la première étape semble de faire des documents *riches* (et non pas nécessairement complexes), qui pourront être utilisés de manière versatile. Gemini marque un point en restant volontairement simple : celà convient à la plupart des cas d'utilisation. Ce document riche d'informations peut donc dans un premier temps être présenté de manière simple par ce service.
Dans un second temps, si d'autres objectifs sont voulu, une chaine éditoriale bien construite permettra de simplement republier les mêmes informations de manières différentes, sans ajouter spécialement de complexité.

## Références

Extrait de l'article [*Le Web est-il devenu trop compliqué ?*](framablog.org/2020/12/30/le-web-est-il-devenu-trop-complique) par Stéphane Bortzmeyer publié sur Framablog le 30/12/2020 

Cours 4, 10 et 11 de l'UV WE01 consultables sur [librecours](https://librecours.net/parcours/we01-20a/sequences.html)

## License
![](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)
Cette œuvre est mise à disposition selon les termes de la [icence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 International](http://creativecommons.org/licenses/by-nc/4.0/)

Cette licence simple autorise la rediffusion et la modification de ce contenu, à condition de me citer et de ne pas en faire une utilisation commerciale.